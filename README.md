# Hanoï Towers

Graduate project of coding Hanoï's Towers in Minizinc.

## Structure of the code

The code uses 3 parameters entered within command lines or prompted when launching the code in Minizinc IDE :

- nbDisc = For the disc number you want to use.
- nbTower = For the tower number you want to use.
- maxStep = The maximum number of step.


The code uses 4 array for declaring our conditions :

- locationDiscInitial = Representing the disc location in the initial state.
- locationDiscFinal = Representing the disc location in the final state.
- heightDiscInitial = Representing the height of each disc in the initial state.
- heightDiscFinal = Representing the height of each disc in the final state.

The code uses 5 decision variables :

- indexMovingDisc = An array representing the discs that moved in each state.
- indexMovingTower = An array representing the towers were the discs moved in each state.
- towerOfAllDiscs = A 2D array representing all the towers of each discs in each state.
- heightOfAllDiscs = A 2D array representing all the height of each discs in each state.
- allDiscsByTowers = A 2D array representing all the discs of each towers in each state.
- nbTurn = The number of turn, will be the minimal number of turn when the algorithm minimizes it.

## Main Algorithm

The easiest and quickest algorithm is to define that each odd number disc goes to the left and each even number disc goes to the right. This rule is right in the whole algorithm, with 3 towers and 3 discs you can see that the disc 3 goes to the left (because there isn't any towers it will go to the last one), the disc 2 goes to the right, the disc 3 goes from the left on top of the disc 2, the disc 1 goes to the left (the last tower) because it's now empty, the disc 3 goes to the left to let the disc 2 going on top to the disc 1, the disc 3 then finally goes to the left on top of the disc 2.

We need to implement this constraint, that an odd disc always goes to the left and if there isn't any towers left needs to go the last one. He can only be on an empty tower or on top of a larger disc (a disc with a lower disc index). It's the same thing for a even disc however instead of going to the left he goes to the right.

We need to initialize the first row of towerOfAllDiscs and the first one of heightOfAllDiscs with locationDisctInitial and heightDiscInitial respectively. For 3 discs and 3 towers it would look like that :

```
locationDiscInitial = [1, 1, 1]
heightDiscInitial 	= [1, 2, 3]
towerOfAllDiscs[1]	= [1, 1, 1]
heightOfAllDiscs[1]	= [1, 2, 3]
```

We need to put in allDiscsByTower a set created of all our discs, with 0 on the other towers. For 3 discs and 3 towers it would look like that :

```
allDiscsByTower[1]	= [[1, 2, 3], [0], [0]]
```

The main loop goes from 2 to the maxStep.
The first step is the initialisation of the game so we don't begin at this one, we already initialized the values previously.
We move the highest disc depending of if it's odd or even. We update indexMovingDisc, indexMoving and allDiscsByTowers with the current disc and the current towers. The stopping condition is 
```
towerAllDiscs[currentIndex] = locationDiscFinal && heightAllDiscs[currentIndex] = heightDiscFinal
```
This condition is simple enough, we want all our discs to be in the last tower and that they all have their final height. If this condition is not met we go to the next step.
